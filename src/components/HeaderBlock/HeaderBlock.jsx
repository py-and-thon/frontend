import React from 'react';
import { Menu, Image, Input, Modal, Button, Grid, Container  } from 'semantic-ui-react';
import VKlogo from '../../images/VK.svg';
import bg from '../../images/bg.jpg';

import NoLogMenu from '../NoLogMenu'
import styles from './styles.module.scss';
import classNames from 'classnames';

const VK = window.VK;

class HeaderBlock extends React.PureComponent {


  
    getPageIcon = () => (
      <Menu.Item position='right'>
        <div>
            <Image src={this.props.user.photo} avatar />
            <span>{this.props.user.first_name} {this.props.user.last_name}</span>
        </div>
      </Menu.Item>
    );


    render(){
        return (
            <Menu inverted color='violet'>
                <Menu.Item className={styles.hackaton} header>Hackathon</Menu.Item>
                <Menu.Menu className={styles.text} position='right'>
                    {this.props.isAuthenticated ? this.getPageIcon() : <NoLogMenu {...this.props}/>}
                </Menu.Menu>
            </Menu>
        );
    }
}

export default HeaderBlock;
