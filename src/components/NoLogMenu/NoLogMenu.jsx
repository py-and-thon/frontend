import React from 'react';
import { Menu, Image, Input, Modal, Button, Grid, Container  } from 'semantic-ui-react';
import VKlogo from '../../images/VK.svg';

import styles from './styles.module.scss';
import classNames from 'classnames';

class NoLogMenu extends React.PureComponent  {
    state = {
        sirId: null
    }

    handleButtonPress = event => {
      console.log('press - ', event.target.value)
      console.log('event.charCode - ', event.charCode)

      if (event.charCode === 13) {
          this.setState({sirId: event.target.value});
          console.log('props - ', this.props)
          this.props.setSirId(event.target.value);
      }
    }

    getModal = () => (
      <Modal size='mini' trigger={<a>Войти или Зарегистрироваться</a>}>
        <Modal.Header>Войти на портал</Modal.Header>
        <Modal.Content >
          <div className="ui equal width grid centered">
            <div className="stretched row">
              <Image wrapped size='small' src={VKlogo} />
            </div>
            <Input onKeyPress={this.handleButtonPress}  />
            <div className="stretched row">
              <Modal.Description>
                {
                  this.state.sirId
                  ? (<Button color='vk' onClick={() => VK.Auth.login(this.props.login, VK.access.PHOTOS)}>
                    Войти
                  </Button>)
                  : null
                }
                <br/>
                На данный момент поддерживается только авторизация через vk
                </Modal.Description>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    );

    render() {
        return (
            <Menu.Item position='right'>
                {this.getModal()}
            </Menu.Item> 
        );
    }
}

export default NoLogMenu;
