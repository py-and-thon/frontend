import React from 'react';

import { Input, Icon, Image, Card, Label } from 'semantic-ui-react';
import styles from './styles.module.scss';
import classNames from 'classnames';

class MainPage extends React.PureComponent  {
    state = {
      tips: null,
      ownEvents: null,
      commonEvents: null
    }

    componentWillMount() {
        console.log('this.props -', this.props);
        if (!this.props.isAuthenticated) this.props.history.push('/');
    }

    tipClick = event => {
      console.log('event - ', event);
      console.log('event target - ', event.target.innerText);
      this.getOwnEvents(event.target.innerText);
      this.setState({tips: null}, ()=> {
        this.getTips()
      })
    }

    getLabelGroup = (tips) => (
      <div className={(classNames(styles.tips))}>
        <span className={(classNames(styles.textS))}>Например:</span>
        <Label.Group color='violet'>
          {tips.map(tip => (<Label basic color='violet' onClick={this.tipClick} as='a'>{tip}</Label>))}
        </Label.Group>
      </div>
    )

    getEventCards = (events, sectionName) => (
      <div className={(classNames(styles.text))}>
        <span>{sectionName}</span>
      <div className={(classNames(styles.cardContainer))}>
        <Card.Group>
        {events.map(event => (
          <Card className={(classNames(styles.card))}>
          {console.log('section2 - ', events)}
          <img className={(classNames(styles.img))} src={event.image_url} />
          <Card.Content>
          <Card.Header>{event.name}</Card.Header>
          <Card.Meta>{event.city}</Card.Meta>
          <Card.Description>{event.description}</Card.Description>
          </Card.Content>
          <Card.Content extra>
            {event.category}
            </Card.Content>
          </Card>
        ))}
        </Card.Group>
      </div>
      </div>
    )

    componentDidMount(){
        var body = document.body;
        console.log('body - ', body);
        body.setAttribute('style', 'background-image: null');
        body.setAttribute('style', 'background-color: #E7E7E7');
        this.getTips();
        this.getCommonEvents();
    }

    getOwnEvents = (query) => {
      var url = 'http://104.248.246.250/api/search';

      var params = {vk_user_id: this.props.id, sirius_user_id: this.props.sirid, q: query} // or:


      fetch(url, {
        method: "GET",
        mode: 'cors'
      })
        .then(function(response) {
            console.log(response.headers.get('Content-Type')); // application/json; charset=utf-8
            console.log(response.status); // 200
            console.log(response.body); // 200

            return response.json();
        })
        .then((events) => {
            console.log('events - ', events.results);
            this.setState({ownEvents: events.results})
        })
        .catch( alert );
    }

    getCommonEvents = () => {
      var url = 'http://104.248.246.250/api/events';

      var params = {vk_user_id: this.props.id, sirius_user_id: this.props.sirid} // or:


      fetch(url, {
        method: "GET",
        mode: 'cors'
      })
        .then(function(response) {
            console.log(response.headers.get('Content-Type')); // application/json; charset=utf-8
            console.log(response.status); // 200
            console.log(response.body); // 200

            return response.json();
        })
        .then((events) => {
            console.log('events - ', events);
            this.setState({commonEvents: events.sections})
        })
        .catch( alert );
    }

    getTips = () => {
      var url = 'http://104.248.246.250/api/tips';

      var params = {vk_user_id: this.props.id, sirius_user_id: this.props.sirid} // or:


      fetch(url, {
        method: "GET",
        mode: 'cors'
      })
        .then(function(response) {
            console.log(response.headers.get('Content-Type')); // application/json; charset=utf-8
            console.log(response.status); // 200

            return response.json();
        })
        .then(({tips}) => {
            this.setState({tips});
        })
        .catch( alert );
    }

    getSections = () => {
        return this.state.commonEvents.map((section) => {
          console.log('section - ', section.events)
          return  this.getEventCards(section.events, section.name)}
      )
    }

    handleKeyPress = () => {
      console.log('press')
      if (event.charCode === 13) {
        this.setState({inputV: event.target.value});
        console.log('event.target.value - ', event.target.value)
        this.getOwnEvents(event.target.value);
        event.target.value = ""
      }
    }

    render() {
      console.log('state - ', this.state)

        return (
            <div className={(classNames(styles.mainContainer))}>
                <div className={(classNames(styles.search))}>
                    <Input onKeyPress={this.handleKeyPress}huge fluid icon='search' placeholder='Search...' />
                    {this.state.tips ? this.getLabelGroup(this.state.tips) : null}
                </div> 
                <div className={(classNames(styles.ownEvents))}>
                  {this.state.ownEvents ? this.getEventCards(this.state.ownEvents) : null}
                </div> 
                <div className={(classNames(styles.ownEvents))}>
                  {this.state.ownEvents ? null : !this.state.commonEvents || this.getSections()}
                </div> 
            </div>
        );
    }
}

export default MainPage;
