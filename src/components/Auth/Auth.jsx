import React from 'react';
import { Container, Grid, Button, Icon, Divider, Segment, Message } from 'semantic-ui-react';

import classNames from 'classnames';
import styles from './styles.module.scss';

import bg from '../../images/bg.jpg';

const VK = window.VK;


class Auth extends React.PureComponent {
    componentWillMount() {
        if(this.props.isAuthenticated) this.props.history.push('/');
    }

    componentDidMount(){
        var body = document.body;
        console.log('body - ', body);
        body.setAttribute('style', "background-image: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('/public/bg.jpg')");
    }
    render () {
        console.log('state - ', this.props);

        return (
            <div className={classNames(styles.container)}>
                <div className={styles.textH}>
                    Образовательный центр «Сириус»
                </div>
                <div className={styles.text}>
                    Был открыт Фондом «Талант и успех» в 2015 году по решению Президента РФ В.В. Путина, возглавляющего его Попечительский Совет.
                    Цель работы Образовательного центра «Сириус» — раннее выявление, развитие и дальнейшая профессиональная поддержка детей, проявивших выдающиеся способности в области искусств, спорта, естественнонаучных дисциплин, а также добившихся успеха в техническом творчестве.
                </div>
            </div>
        );
    }
};

export default Auth;
