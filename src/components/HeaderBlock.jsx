import React from 'react';
import { Menu, Image, Input, Modal, Button, Grid, Container  } from 'semantic-ui-react';
import VKlogo from '../../images/VK.svg';
import bg from '../../images/bg.jpg';

import styles from './styles.module.scss';
import classNames from 'classnames';

const VK = window.VK;

class HeaderBlock extends React.PureComponent {
    getModal = () => (
      <Modal size='mini' trigger={<a>Войти или Зарегистрироваться</a>}>
        <Modal.Header>Войти на портал</Modal.Header>
        <Modal.Content >
          <div className="ui equal width grid centered">
            <div className="stretched row">
            <Image wrapped size='small' src={VKlogo} />
            </div>
            <Input placeholder='Введите ваш Сириус ID' />
            <div className="stretched row">
              <Modal.Description>
                <Button color='vk' onClick={() => VK.Auth.login(this.props.login, VK.access.PHOTOS)} >
                  Войти
                </Button>
                <br/>
                На данный момент поддерживается только авторизация через vk
                </Modal.Description>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    );

  
    getPageIcon = () => (
      <Menu.Item position='right'>
        <div>
            <Image src={this.props.user.photo} avatar />
            <span>{this.props.user.first_name} {this.props.user.last_name}</span>
        </div>
      </Menu.Item>
    );

    getNoLogMenu = () => (
        <Menu.Item position='right'>
            {this.getModal()}
        </Menu.Item> 
    );

    render(){
        return (
            <Menu inverted color='violet'>
                <Menu.Item className={styles.hackaton} header>Hackathon</Menu.Item>
                <Menu.Menu className={styles.text} position='right'>
                    {this.props.isAuthenticated ? this.getPageIcon() : this.getNoLogMenu()}
                </Menu.Menu>
            </Menu>
        );
    }
}

export default HeaderBlock;
