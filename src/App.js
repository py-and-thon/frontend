import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Auth from './components/Auth';
import MainPage from './components/MainPage';
import HeaderBlock from './components/HeaderBlock';

import sessionControlHoc from './hocs/sessionControlHoc';

import logo from './logo.svg';
import './App.css';

const App = (props) => (
    <div className="App">
        { console.log('auth? - ', props.isAuthenticated)}
        <HeaderBlock {...props}/>
        <Route exact
            path="/auth"
            {...props}
            render={() => <Auth {...props}
                login={props.login}
                logout={props.logout}
            />}/>
        <Route exact
            path="/main"
            {...props}
            render={() => <MainPage {...props} />}/>
        <Route exact path='/' component={() => {
            return props.isAuthenticated ? (
                <Redirect to='/main'/>
            ) : (
                <Redirect to={{
                    pathname: '/auth',
                }} />
            );
        }}/>
    </div>
);

export default sessionControlHoc(App);
