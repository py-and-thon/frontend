import React from 'react';
import { withRouter } from 'react-router';

const VK = window.VK;

const isExpired = () => {
  const sessionInfo = JSON.parse(window.sessionStorage.getItem('sessionInfo'));
  
  if (!sessionInfo) return true;

  if ((Date.now() - sessionInfo.session.expire) < 0) return false;

  return false;
};

function sessionControlHoc(WrappedComponent) {
  const hoc = class SessionControlHoc extends React.PureComponent {
    constructor() {
      super();
      this.state = {
        isAuthenticated: false,
        user: null,
      };
    }

    componentWillMount() {
      const sessionInfo = JSON.parse(window.sessionStorage.getItem('sessionInfo'));

      if (sessionInfo !== null && typeof sessionInfo === 'object' && !isExpired()) {
        const { session: {user: { first_name, last_name, id, sirid }}, photo} = sessionInfo;

        console.log('sessionInfo exist', photo)
        this.setState(() => {
          return {
            isAuthenticated: true,
            user: {
              id,
              first_name,
              last_name,
              photo,
              sirid
            },
          };
        });
      } else {
        console.log('sessionInfo no exist')
      }
    }

    componentWillUpdate() {
      if (isExpired()) {
        this.setState(() => {
          return {
            isAuthenticated: false,
          };
        });
      }
    }

    setSirId = (sirid) => {
      console.log('sirid - ', sirid)
      this.setState(() => {
        return {
          user: {
              ...this.state.user,
              sirid
          },
        };
    })}

    login = (info) => {
      console.log(info)
      const { session: {user: { first_name, last_name, id }}} = info;
      window.sessionStorage.setItem('sessionInfo', JSON.stringify(info));
      this.setState(() => {
        return {
          isAuthenticated: true,
          user: {
              ...this.state.user,
              id,
              first_name,
              last_name
          },
        };
      });
      this.getPhoto(id);
      this.props.history.push("/");
    }

    getPhoto(id) {
      VK.api('users.get', {
          user_ids: id,
          fields: 'photo_50',
          mode: 'no-cors',
          version: 5.87
      }, (data) => {
          const photo_50 = data.response[0].photo_50;
          this.setState(() => {
            return {
              user: {
                ...this.state.user,
                photo: photo_50,
              }
            };
          });
          const sessionInfo = JSON.parse(window.sessionStorage.getItem('sessionInfo'));
          sessionInfo.photo = photo_50;
          window.sessionStorage.removeItem('sessionInfo');
          window.sessionStorage.setItem('sessionInfo', JSON.stringify(sessionInfo));
      });
    }

    logout = () => {
      window.sessionStorage.removeItem('sessionInfo');
      this.setState(() => {
        return {
          isAuthenticated: false,
          user: null,
        };
      });
    }

    render() {
      return (
        <WrappedComponent
          isAuthenticated={ !isExpired() && this.state.isAuthenticated }
          login={this.login}
          logout={this.logout}
          setSirId={this.setSirId}
          user={this.state.user}
          {...this.state}
          {...this.props}>
          {this.children}
        </WrappedComponent>
      );
    }
  };
  return withRouter(hoc);
};

export default sessionControlHoc;
